//
//  sensor.h
//  NaviBeesModel
//
//  Created by ahmed eshra on 4/14/15.
//  Copyright (c) 2015 uqu. All rights reserved.
//

#ifndef __NaviBeesModel__sensor__
#define __NaviBeesModel__sensor__

#include <stdio.h>
#include "kalman.h"

KalmanFilter alloc_filter_sensor(int state_dimension, int observation_dimension, double dt);

/* Set the seconds per timestep in the velocity2d model. */
void set_seconds_per_timestep_sensor(KalmanFilter f,
                                     double seconds_per_timestep);

/* Update the kalman value with new data. */
void update_sensor(KalmanFilter f, double newValue,
                   double seconds_since_last_update);

/* Extract sensor value from a Kalman filter. */
void get_sensorValue(KalmanFilter f, double* value);

#endif /* defined(__NavInside__sensor__) */

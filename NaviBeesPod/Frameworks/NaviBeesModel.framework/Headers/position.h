//
//  position.h
//  NavInside
//
//  Created by ahmed eshra on 4/9/15.
//  Copyright (c) 2015 uqu. All rights reserved.
//

#ifndef __NaviBeesModel__position__
#define __NaviBeesModel__position__

#include <stdio.h>
#include "kalman.h"

KalmanFilter alloc_filter_position(int state_dimension, int observation_dimension, double dt);

/* Set the seconds per timestep in the velocity2d model. */
void set_seconds_per_timestep(KalmanFilter f,
                              double seconds_per_timestep);

/* Update the kalman points with new data. */
void update_point(KalmanFilter f, double lat, double lon,
                  double seconds_since_last_update);

/* Extract x and y coordinate from a Kalman filter. */
void get_coordinate(KalmanFilter f, double* lat, double* lon);

/* Reset kalman variables */
void reset_kalman(KalmanFilter f);

#endif /* defined(__NavInside__position__) */

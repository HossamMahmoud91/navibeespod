//
//  NBLocationProviderWrapper.h
//  NaviBees
//
//  Created by Hossam M. Saad on 1/26/16.
//  Copyright © 2016 uqu. All rights reserved.
//

#ifndef NBMyLocationProviderWrapper_h
#define NBMyLocationProviderWrapper_h

#import <Foundation/Foundation.h>

@class IndoorLocation;

@interface NBMyLocationProviderWrapper : NSObject

-(id) init;
-(void) enable;
-(void) disable;
-(void) sendLocation: (IndoorLocation*)location withAccuracy:(int)accuracy indoorFlag:(bool) isIndoor;
-(void) sendCompassHeading: (double)heading;
-(void) setCompassMode: (bool) enabled;
-(bool) isEnabled;

@end

#endif /* NBMyLocationProviderWrapper_h */

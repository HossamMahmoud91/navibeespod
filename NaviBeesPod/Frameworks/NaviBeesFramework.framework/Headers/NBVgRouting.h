//
//  NBVgRouting.h
//  NaviBeesFramework
//
//  Created by Hossam M. Saad on 4/10/17.
//  Copyright © 2017 NaviBees. All rights reserved.
//

#ifndef NBVgRouting_h
#define NBVgRouting_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class POI;
@class IndoorLocation;

@interface NBVgRouting : NSObject

    +(NSArray<POI*>*) getNearestPOIsFromPOI:(POI *)poi inList:(NSArray<POI*>*)poisList withMaxDistance:(double) maxDistance;
    +(NSArray<POI*>*) getNearestPOIsFromLocation:(IndoorLocation*)location inList:(NSArray<POI*>*)poisList withMaxDistance:(double) maxDistance;
@end

#endif /* NBVgRouting_h */

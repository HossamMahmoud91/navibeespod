/*
 * Copyright 2012, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */

#import <UIKit/UIKit.h>


#import "VgMyConfigPreferences.h"
#import "VgMyPassThroughView.h"




@class VgEAGLView;
@class VgMyBubbleView;

@class VgMyRemoteMapManager;
@class VgMyCompassView;
@class VgMyCompassDataSource;
@class FloorPickerView;
@class POI;
@class Facility;
@class Floor;
@class Building;
@class IndoorLocation;

@protocol MapHoverDelegate;


@interface MapViewController: UIViewController
{
    VgEAGLView* mMapView;
    
//    UISwitch* mSimulatePositionSwitch;
//    UISwitch* mAvoidStairsRoutingSwitch;
    
//    UILabel* mSimulatePositionLabel;
//    UILabel* mAvoidStairsRoutingLabel;
    
//    UIButton* mTogglePoisButton;
//    UIButton* mResetPreferencsButton;
//    UIButton* mClearRouteButton;

    UIProgressView* mProgressView;
    
    VgMyConfigPreferences* mConfigPreferences;
    
    bool mTogglePointsVisible;

    BOOL mMapIsLoaded;
    
    id <MapHoverDelegate> mapHoverDelegate;

    // these can be used by external controllers to change the map that gets loaded
    BOOL mShouldTryLoadFromNetwork; // default YES
    BOOL mShouldTryEmbeddedMap; // default YES
    BOOL mShouldTryLoadMapFromPreferences; // default YES

}

@property (retain) id <MapHoverDelegate> mapHoverDelegate;

@property (readwrite) BOOL mShouldTryLoadFromNetwork;
@property (readwrite) BOOL mShouldTryEmbeddedMap;
@property (readwrite) BOOL mShouldTryLoadMapFromPreferences;

@property (readonly) BOOL mMapIsLoaded;

@property (retain) IBOutlet VgEAGLView* mMapView;
@property (retain) VgMyBubbleView* currentCallout;
//@property (retain) IBOutlet UISwitch* mSimulatePositionSwitch;
//@property (retain) IBOutlet UISwitch* mAvoidStairsRoutingSwitch;
@property (retain) UIProgressView* mProgressView;

//@property (retain) IBOutlet UILabel* mSimulatePositionLabel;
//@property (retain) IBOutlet UILabel* mAvoidStairsRoutingLabel;

//@property (retain) IBOutlet UIButton* mTogglePoisButton;
//@property (retain) IBOutlet UIButton* mResetPreferencsButton;
//@property (retain) IBOutlet UIButton* mClearRouteButton;
//@property (retain) IBOutlet UIButton* mCompassButton;
//@property (retain) IBOutlet UIView* mCompassContainer;
//@property (retain) IBOutlet NSLayoutConstraint* mCompassVisibleConstraint;
//@property (retain) IBOutlet NSLayoutConstraint* mCompassHiddenConstraint;

@property (retain) VgMyConfigPreferences* mConfigPreferences;

@property (copy) NSString* mRemoteMapName;
@property (retain) NSObject* mRemoteMapManagerConfig; // should be either VgMyRemoteMapManagerConfigLoginPassword (deprecated) or VgMyRemoteMapManagerConfigHash

@property (strong) VgMyRemoteMapManager* mVgMyRemoteMapManager;

//@property (strong) VgMyCompassView* mCompassView;
//@property (strong) VgMyCompassDataSource* mCompassDataSource;
//@property (strong) IBOutlet UIView* mSelectorViewContainer;
//@property (strong) IBOutlet VgMyPassThroughView* mSelectorTableContainer;
-(void)setRemoteMapName:(NSString*)mapName serverURL:(NSString*)serverURL hash:(NSString*)hashname indexFilename:(NSString*)descriptor;
-(void)setRemoteMapName:(NSString*)mapName serverURL:(NSString*)serverURL userName:(NSString*)userName password:(NSString*)password;


-(void) updateMapMetadata;
-(void) currentLocationAction;
-(void) setCompassMode: (BOOL) enable;
-(void) floorActionButton:(BOOL)opened;
-(void) buildingActionButton:(BOOL)opened;
-(void) didSelectFloor:(Floor*) floor;
-(void) didSelectBuilding:(Building*)building;
-(void) mapDirectionsRouteTo:(POI*)element;
-(POI*) mapDirectionsFindNearestFacility:(Facility*)facility;
-(double) selectPOI:(POI*) selectedPOI;
-(double) selectLocation:(IndoorLocation*) location;
-(void) diselectPOI;
-(void) startNavigationToSelectedPOI:(double*)returnTime distance:(double*)returnDistance;
-(void) endNavigation;
-(Floor*) getFloorOfPOI:(POI*)poi;
-(void) showRealLocation:(BOOL) show;

-(void) clearMarkers;

-(void) zoomToUTMLocation:(IndoorLocation*) location stopAutoMode:(BOOL) stop;
-(void) zoomToLongLatLocation:(IndoorLocation*) location stopAutoMode:(BOOL) stop;

-(void) setLocationProvider:(BOOL)mode;
/**
 * Configure the remote map properties and start the download process
 * @see For more information, see the following blog which explains
 * how your app can download automatically your map:
 * https://developer.visioglobe.com/blog/2013/08/29/integrating-your-map-bundle/
 */
-(IBAction) loadFromNetwork:(id)sender;

-(IBAction) resetConfigPreferences:(id)sender;

-(IBAction) togglePOIs:(id)sender;


- (IBAction)mapLongPressAction:(UILongPressGestureRecognizer *)sender;

/**
 * Compares two map bundle versions and determines whether the first version is
 * older then the second version.
 * @returns Returns YES, if pVersion1 is older than pVersion2, otherwise NO.
 */
+(Boolean)isVersion:(NSString*)pVersion1 olderThan:(NSString*)pVersion2;

-(BOOL)loadMap:(VgMyConfigPreferences*)pConfig;
- (void)unloadMap;

/**
 * This needs to be called when ever the mMapView changes size
 * to update the stackview parameters
 */
-(void)updateMapViewParameters;

-(void)startProgress;
-(void)progress:(int)pReceived outOf:(int)pTotal;
-(void)doneProgress;

/**
 * Assuming the map has been loaded, it center the camera above a given place ID
 */
-(void)gotoPlaceId:(NSString*)pPlaceId;

/**
 * Block to be called once the map has been fully loaded
 * @example
 [lMyViewController onMapLoaded:^(VgMyViewController* pMyViewController)
 {
    [pMyViewController gotoPlaceId:@"B3-UL01-ID0074"];
 }];
 */
-(void)onMapLoaded:(void(^)(MapViewController* pMyViewController))pBlock;

/*
 * This function is used for debugging purposes to active a route and a bubble at the start.
 */
-(void)automaticTest;

#pragma mark - Used by VisioPreview

@end

/*
 * Copyright 2013, Visioglobe SAS
 * ALL RIGHTS RESERVED
 *
 *
 * LICENSE: Visioglobe grants the user ("Licensee") permission to reproduce,
 * distribute, and create derivative works from this Source Code,
 * provided that: (1) the user reproduces this entire notice within
 * both source and binary format redistributions and any accompanying
 * materials such as documentation in printed or electronic format;
 * (2) the Source Code is not to be used, or ported or modified for
 * use, except in conjunction with Visioglobe SDK; and (3) the
 * names of Visioglobe SAS may not be used in any
 * advertising or publicity relating to the Source Code without the
 * prior written permission of Visioglobe.  No further license or permission
 * may be inferred or deemed or construed to exist with regard to the
 * Source Code or the code base of which it forms a part. All rights
 * not expressly granted are reserved.
 *
 * This Source Code is provided to Licensee AS IS, without any
 * warranty of any kind, either express, implied, or statutory,
 * including, but not limited to, any warranty that the Source Code
 * will conform to specifications, any implied warranties of
 * merchantability, fitness for a particular purpose, and freedom
 * from infringement, and any warranty that the documentation will
 * conform to the program, or any warranty that the Source Code will
 * be error free.
 *
 * IN NO EVENT WILL VISIOGLOBE BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT
 * LIMITED TO DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES,
 * ARISING OUT OF, RESULTING FROM, OR IN ANY WAY CONNECTED WITH THE
 * SOURCE CODE, WHETHER OR NOT BASED UPON WARRANTY, CONTRACT, TORT OR
 * OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY PERSONS OR
 * PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED FROM,
 * OR AROSE OUT OF USE OR RESULTS FROM USE OF, OR LACK OF ABILITY TO
 * USE, THE SOURCE CODE.
 *
 * Contact information:  Visioglobe SAS,
 * 55, rue Blaise Pascal
 * 38330 Monbonnot Saint Martin
 * FRANCE
 * or:  http://www.visioglobe.com
 */
#import <UIKit/UIKit.h>

/**
 * Holds information about the current configuration:
 * - Current map configuration path
 * - Credentials
 * - Version of the map.
 */
@interface VgMyConfigPreferences : NSObject
{
    /**
     * The path of the configuration file for the map bundle which is to be loaded.
     */
    NSString* configurationFile;
    
    /**
	 * The secret code which links the license to the map bundle
	 */
    NSInteger secret;
    
    /**
	 * The URL of the Visioglobe license server, which the VisioMove SDK will use to request
	 * a license (if none already exist within the map bundle).
	 */
    NSString* licenseURL;
    
    /**
	 * The version of the map bundle.
	 */
    NSString* version;
}
@property (nonatomic, retain, setter = setConfigurationFile:, getter = getConfigurationFile) NSString* configurationFile;
@property (readwrite) NSInteger secret;
@property (copy) NSString* licenseURL;
@property (copy) NSString* version;

/**
 * Sets the configuration name.
 *
 * @param pConfigurationFile - The path to the map bundle's configuration file {PATH}/vg_config.xml.
 * The path may be either:
 * - a path absolute OR
 * - a path relative to the Application directory {APPLICATIONS_DIRECTORY}/{GUID_DIRECTORY}/{NAME_OF_APP}.app/ OR
 * - a path relative to the Application Support/Visioglobe directory
 *
 * In all cases, the path will be stored, in shared preferences, as a relative path to the Application Support path.
 * This ensures the configuration file path remains valid if the {GUID_DIRECTORY} changes, which is the case during an
 * application upgrade.
 */
-(void)setConfigurationFile:(NSString*)pConfigurationFile;

/**
 * @return Retrieves the full path of the configuration file.
 * @remark The full path can be either 
 * - an absolute path starting with {Application Support directory}/Visioglobe if the file exists OR
 * - an absolute path starting with {Application Bundle path} by default.
 */
-(NSString*)getConfigurationFile;

/**
 * @return the path where Visioglobe's data are stored.
 */
+(NSString*)getVisioglobeDataDirectory;

/**
 * The contents of the VisioglobeDataDirectory live in the Applications Support
 * directory which is backed up by iCloud.  Apple sometimes consider map data
 * as something that can be recreated, thus that it should not be backed up.
 * This method marks all the files in the VisioglobeData directory not to
 * be backed up.
 */
+(void)markVisioglobeDataDirectoryDoNotBackup;

/**
 * Moves downloaded licenses stored in the old directory structure to the new one (next to their vg_config.xml)
 *
 * @return True if all licenses have been moved successfully.
 */
+(BOOL)moveLicenseFiles;

/**
 * Saves the current object in the [NSUserDefaults standardUserDefaults].
 */
-(void)save:(NSString*)venueId;

/**
 * @return the last saved VgMyConfigPreferences or nil if none.
 */
+(id)getFromPreferences:(NSString*)venueId;

/**
 * Resets the preferences such that the next "getFromPreferences" returns nil.
 */
+(void)reset;
@end

//
//  NaviBeesPod.swift
//  NaviBeesPod
//
//  Created by Hossam M. Saad on 4/3/17.
//  Copyright © 2017 NaviBees. All rights reserved.
//

import Foundation
import NaviBeesModel
import NaviBeesFramework

public class NaviBeesPod : NSObject {
    
    open static func tryNaviBeesModel() {
        AppManager.sharedInstance.initialize(application: UIApplication.shared)
        
        let venues = AppManager.sharedInstance.getMapMetadataManager().getVenues()
        
        for (_, venue) in venues {
            print("venue name= \(venue.name)")
        }
    }
    
    open static func initNaviBees(debug: Bool){
        if debug {
            AppManager.sharedInstance.initialize(application: UIApplication.shared, buildConfig: .debug)
        }else{
            AppManager.sharedInstance.initialize(application: UIApplication.shared)
        }
    }
    
    open static func setCurrentVenue(venueId : String){
        AppManager.sharedInstance.getMapMetadataManager().setCurrentVenue(venueId: venueId)
    }
    
    open static func openNaviBees() -> UIViewController? {
        let navibeesStoryBoard = UIStoryboard(name: "NaviBees", bundle: NaviBeesFramework.bundle)
        let viewController = navibeesStoryBoard.instantiateInitialViewController()
        return viewController
    }
    
    open static func getCategories() -> [POIsCategory]? {
        return AppManager.sharedInstance.getMapMetadataManager().getCurrentVenue()?.getCategories()
    }
    
}
